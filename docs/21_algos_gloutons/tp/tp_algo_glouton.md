---
title: "Chapitre 21 : Algorithmes Gloutons "
subtitle: "TP"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

# Exercice 1 : QCM   &#x1F3C6;

__a.__ Quand utilise-t-on principalement des algorithmes gloutons ?  

* [ ] lorsqu'il y a un problème d'optimisation et que le dénombrement des solutions est trop long.
* [ ] quand il y a beaucoup de possibilités sans solution.
* [ ] quand il y a plus de 10 opérations.
* [ ] Jamais

__b.__ Quand les algorithmes gloutons donnent-ils la solution optimale ?  

* [ ] toujours
* [ ] Dans certains problèmes d'optimisation et sous certaines conditions
* [ ] Dans les problèmes de type voyageur de commerce
* [ ] Jamais

__c.__ Comment appelle-t-on dans un algorithme glouton un choix local qui nous parait optimal mais qui ne conduit pas forcément à une solution globale optimale ?  

* [ ] un échec
* [ ] Pas de nom particulier
* [ ] une heuristique gloutonne
* [ ] un glouton non optimal

__d.__ On dispose en quantité illimité de pièces de 1 euro, 2 euros et 5 euros. On veut totaliser une somme de 18 euros. Quelle est la solution donnée par l’algorithme glouton ?

* [ ] `[5, 5, 5, 2, 1]`
* [ ] `[5, 5, 5, 2, 2, 1]`
* [ ] `[5, 5, 2, 2, 2, 1, 1]`
* [ ] `[5, 2, 2, 2, 2, 1, 1, 1, 1, 1]`

# Exercice 2 : Problème du sac à dos &#x1F3C6;
(_Exercice inspiré du livre NSI édité chez ellipses_)

Un voleur dévalisant un magasin à devant lui un ensemble d'objets de masse et de valeur différentes. Son sac à dos peut supporter une masse maximale de 10kg.  
Quels objets prendre pour que la valeur en euros dans le sac à dos soit la plus grande possible en ne dépassant pas la masse maximale autorisée.  

![Problème du sac à dos](./fig/cambrioleur.png)

_source image : https://www.gratispng.com/png-ggkwql/_

__Cas n°1 :__

| Objet | A | B |C |D|
|:------:|:------:|:------:|:------:|:------:|
| masse (kg) | 8| 5 |4 |1|
| prix (€) | 4800 | 4000 |3000 |500|

__Cas n°2 :__

| Objet | A | B |C |D|
|:------:|:------:|:------:|:------:|:------:|
| masse (kg) | 6| 5 |4 |1|
| prix (€) | 4800 | 3500 |3000 |500|

__Cas n°3 :__

| Objet | A | B |C |D|E|
|:------:|:------:|:------:|:------:|:------:|:------:|
| masse (kg) | 9| 6 |5 |4|1|
| prix (€) | 8100 | 7200 |5500 |4000|800|

__Cas n°4 :__

| Objet | A | B |C |D|E|F|
|:------:|:------:|:------:|:------:|:------:|:------:|:------:|
| masse (kg) | 7| 6 |4 |3|2|1|
| prix (€) | 9100 | 7200 |4800 |2700|2600|200|

__a)__ Pour chacun des 4 cas ci-dessus : trouver l'ensemble des combinaisons d'objets possibles pour atteindre une masse la plus proche de 10kg (sans la dépasser).   
Attention chaque objet (A, B, C, D, E, F) ne peut être utilisé qu'une seule fois !

__b)__ A partir d'une résolution naïve et en vous servant du __a)__ trouver la solution optimale dans chaque cas.  

__c)__ On applique maintenant un algorithme de type glouton avec pour règle locale :  
__On choisit les objets qui ont la plus grande valeur à chaque étape et on vérifie que la masse totale ne dépasse pas la valeur de 10 kg.__  

Trouver la solution apportée par l'algorithme glouton dans chaque cas et comparer cette valeur à celle du __b).__  

__d)__ Même question que __c)__ mais on change la règle locale :  
__On choisit les objets qui ont la plus grande masse à chaque étape et on vérifie que la masse totale ne dépasse pas la valeur de 10 kg.__  

Trouver la solution apportée par l'algorithme glouton dans chaque cas et comparer cette valeur à celle du b).  

__e)__ Même question que __c)__ mais on change la règle locale :  
__On choisit les objets qui ont le rapport $\frac{valeur}{masse}$ le plus grand à chaque étape et on vérifie que la masse totale ne dépasse pas la valeur de 10 kg.__  

Trouver la solution apportée par l'algorithme glouton dans chaque cas et comparer cette valeur à celle du __b).__  

# Exercice 3: Problème du rendu de monnaie  &#x1F3C6;&#x1F3C6;

Un  achat  dit  en  espèces  se  traduit  par  un  échange  de  pièces  et  de  billets.  Dans  la  suite,  les  pièces  désignent  indifféremment les véritables pièces que les billets. Supposons qu’un achat induise un rendu de 49 euros. Quelles pièces peuvent être rendues ? La réponse, bien qu’évidente, n’est pas unique. Quatre pièces de 10 euros, 1 pièce de 5 euros et deux pièces de 2 euros conviennent. Mais quarante-neuf pièces de 1 euros conviennent également !    
Si la question est de rendre la monnaie avec un minimum de pièces, le problème change de nature. C’est le problème du rendu de monnaie dont la solution dépend du système de monnaie utilisé.[^1] 

![Monnayeur](./fig/monnayeur.jpg)

__a)__ Donner la solution optimale utilisant un algorithme glouton dans le cas d'un rendu de 49 euros.

__b)__ Modéliser le système monétaire français par un tuple d'entiers nommé `systeme_monnaie`

__c)__ Implémenter l'algorithme glouton pour le rendu de monnaie en définissant une fonction `pieces_a_rendre` acceptant deux paramètres :

* `somme_a_rendre` un entier représentant le rendu total à effectuer
* `systeme_monnaie` un tuple définit dans la question précédente

Cette fonction devra renvoyer la liste des pièces à rendre.   
L'idée générale est de rendre toujours la plus grande pièce possible jusqu'à atteindre la somme souhaitée (Vous pourrez utiliser une boucle pour parcourir le systeme monétaire pour chaque choix de pièce)  

__d)__ Mettre en place une docstring complète et des doctests

# Exercice 4 : Planning d'occupation  &#x1F3C6;&#x1F3C6;&#x1F3C6;

Pour la fête du 14 juillet, un village décide de mobiliser l'ensemble de ses associations. Le maire souhaite dans la salle des fêtes que les associations se relaient de 6h du matin jusque minuit.  

Pour cela, il a demandé à chaque association de proposer un créneau horaire et une activité.  
On peut représenter ces activités sur un schéma d'intervalles[^2] :

![Exemple de planning des activités](./fig/planning.png)  

Chaque activité est caratérisée par un intervalle $[d_{i},f_{i}]$ où $d_{i}$ représente l'heure de début et $f_{i}$ l'heure de fin

A partir du document que le maire vous a donné, vous allez devoir proposer un planning qui comportera un maximum d'activités dans la plage horaire 6h minuit. La seule contrainte est qu'il ne peut y avoir qu'une activité à la fois dans la salle des fêtes. On pourra aussi supposer que les activités des associations peuvent s'enchainer.  

|__Début $d_{i}$__|__Fin $f_{i}$__|__Activité__|
|:---:|:---:|:---:|
|7H|10H|Club aeronautique|
|6H|12H|Athlétisme|
|9H|11H|Danse|
|18H|19H|Zumba|
|14H|17H|Volley-ball|
|14H|18H|Basket|
|9H|19H|Exposition de peinture|
|12H|16H|Tennis|
|11H|15H|Badmington|
|10H|14H|Gymnastique|
|11H|13H|Produits du terroir|
|19H|22H|Belote|
|21H|23H|Pétanque|
|22H|00H|Bal du village|

__1.__ Pour commencer, créer une structure en python `liste_activites` qui stocke l'ensemble des informations données par le maire. Ce sera une liste de tuples. Chaque tuple sera de la forme  `(heure début, heure fin, nom de l'activité)` avec les  types correspondants `(int,int,str)`.  
  
On préfèrera 24h plutôt que 00h pour la fin de l'activité Bal du village.    

Pour résoudre ce problème nous pourrions dénombrer l'ensemble des solutions mais la tâche serait fastidieuse car il y a beaucoup d'activités.  
Nous allons donc aborder le problème non pas de façon globale mais locale. Nous allons choisir une solution locale qui nous semble "localement optimale" en espérant qu'elle sera la solution "optimale globale". On dit qu'on réalise une heuristique gloutonne.  

## Premiere méthode : choisir les activités par ordre croissant de leur horaire de début.  

__2.__ Créer une fonction `tri_debut` sans paramètre qui renvoie un tuple comportant une liste d'activités retenues et un entier représentant la longueur de cette liste. Vous créerez la fonction en suivants les étapes décrites ci-après :     

__a-__ Trier votre structure créée en __1.__ par ordre croissant de leur horaire de début. On appelera cette liste `liste_activites_debut`.  

\newpage

> __Aide__:  La fonction `sorted()` est une fonction en python qui renvoie une liste triée.  
> Elle prend en premier paramètre le nom de la liste à trier, en deuxième paramètre une clé.  
> Cette clé aura pour affectation une fonction anonyme __lambda__ qui permettra de fixer par rapport à quoi nous voulons trier notre liste.  
> _Par exemple:_  
> 
> ``` python
> >>> liste_a_trier = [(2, 5), (5, 3), (4, 1)]
> >>> liste_triee = sorted(liste_a_trier, key = lambda element : element[1])  
> >>> print(liste_triee)
> [(4, 1), (5, 3), (2, 5)]
> ```
> 
> Dans la fonction `sorted`, `element` désigne chaque tuple de la liste.  
>  La liste a été triée par rapport au deuxième nombre de chaque tuple.   
> 
> _Remarque_ : il possible d'ajouter un troisième paramètre `reverse` à la fonction `sorted`.  
> Par défaut s'il n'est pas mentionné il vaut `False`. Si vous voulez trier par ordre décroissant, vous mettrez `reverse = True`.  

__b-__ Créer une liste vide :  `liste_courante`

__c-__ Ajouter un par un les éléments de `liste_activites_debut` dans la liste vide en vérifiant que la contrainte de non chevauchement des activités soit respectée. On commencera par le premier élément.  

__d-__ Lorsque vous avez fini de balayer `liste_activites_debut`, trouver le nombre d'activités dans la liste.  

__e-__ Afficher un tuple comportant la liste des activités et le nombre d'activités.  

``` python  
>>> tri_debut()  
([(6, 12, 'Athlétisme'), (12, 16, 'Tennis'), (18, 19, 'Zumba'), 
(19, 22, 'Belote'), (22, 24, 'Bal du village')], 5)  
```

## Deuxième méthode : choisir les activités par ordre décroissant de leur horaire de fin.  

__3.__ Créer une fonction `tri_fin` en vous servant des idées déjà développées dans le __2.__  
Vous afficherez un tuple comportant la liste des activités sélectionnées et leur nombre.  

``` python  
>>> tri_fin()  
([(22, 24, 'Bal du village'), (19, 22, 'Belote'), (18, 19, 'Zumba'), 
(14, 18, 'Basket'), (10, 14, 'Gymnastique'), (7, 10, 'Club aéronautique')], 6)  
```  

Quelle solution locale est la meilleure ?  

##  Pour aller plus loin (facultatif) 🚀

On peut aussi prendre comme heuristique la __durée croissante des activités__ : définissez une fonction `tri_duree`  

_AIDES:_   
 
* Pour vérifier si la contrainte de non chevauchement est bien respectée, il faut sélectionner un élément de `liste_activites_debut` et le comparer avec les éléments de `liste_courante`.
* On créera donc une boucle "pour" pour prendre un élément de `liste_activites_debut` et dans cette boucle "pour", on réalise une deuxième boucle "pour" afin de sélectionner les éléments de `liste_courante`. 
* On peut ensuite comparer les deux éléments entre eux.

[^1]: https://cache.media.eduscol.education.fr/file/NSI/76/4/RA_Lycee_G_NSI_algo-gloutons_1170764.pdf
[^2]: https://www.apprendre-en-ligne.net/info/algo/algorithmique.pdf
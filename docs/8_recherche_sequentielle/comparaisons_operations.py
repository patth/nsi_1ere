from timeit import timeit

i = 1000000 # nombre d'itérations

a = timeit('x = 5', number = i)
b= timeit('5 > 3', number = i)
c = timeit('2 * 2', number = i)
d = timeit('2 + 4', number = i)
e = timeit('2 / 4', number = i)


print('affectation : ', a /i )
print('comparaison : ', b/ i)
print('multiplication : ', c/ i)
print('addition : ', d/i )
print('division : ', e/i )

---
title: "Chapitre 6  Architecture des ordinateurs"  
subtitle: "PARTIE 2 - Cours : Modèle de Von Neumann"
papersize: a4  
geometry: margin=1.5cm  
fontsize: 12pt  
lang: fr  
---  

Depuis plus de soixante-dix ans, l'architecture des ordinateurs repose sur un modèle dit "de Von Neumann"

# L'architecture von Neumann  

Avant 1945, les premiers ordinateurs n'étaient pas réellement programmables, ils éxécutaient un seul programme câblé dans l’ordinateur (Chaque exécution d’un nouveau programme nécessitait de recâbler l’ordinateur ou de changer une carte, un ruban ...)

![Carte perforée 80 colonnes ( modèle mis au point par IBM en 1928)[^1] ](./fig/punched_card.jpg)    

https://www.youtube.com/watch?v=MDQHE0W-qHs

Dans les années 1945, __John Von Neumann__ définit un modèle de machine entierement nouvelle de part sa conception. L'idée est de stocker les données et les instructions grâce à l'électronique à l'intérieur même de la machine.  

![modèle originel de von Neumann](./fig/modele-originel2.gif)

> 📢 L’architecture des ordinateurs à programme enregistré peut se décomposer en quatre types de composants :  
> 
>   * Une unité de commande (ou contrôle (__UC__))
>   * Une unité arithmétique et logique (__UAL__)  
>   * Une mémoire
>   * Des périphériques d’entrées-sorties
  
Un documentaire d'Arte sur la vie très riche de John von Neumann est disponible (_Regarder surtout entre 39:12 et 46:00_) 
https://www.youtube.com/watch?v=c9pL_3tTW2c  

## La mémoire 

### La mémoire principale

__Elle contient les instructions du ou des programmes en cours d'exécution et les données associées à ce programme__
Cette mémoire peut être vue comme un tableau de cases mémoires élémentaires appleées __mot mémoire__ (de taille pouvant varier de 8 à 64 bits). __Chaque case possède une adresse unique__ à laquelle on se référe pour accéder à son contenu.  

Les échanges entre le processeur et la mémoire se font à travers des dispositifs nommés __bus__.  

![Les différents bus](./fig/bus.jpg) 

### Les différents types de mémoire

On distingue souvent deux grandes catégories de mémoire  

> 📢 la `mémoire morte` ou `ROM` pour _Read Only Memory_ : une mémoire non volatile dont le contenu est fixé  lors de la fabrication. Elle est destinée à être lue plusieurs fois et n'est pas prévue pour être modifiée (_sauf dans certains cas_). 
  
> 📢la `mémoire vive` ou `RAM` pour _Random Access Memory_ : une mémoire volatile dont le contenu disparait s’il n’y a plus d’alimentation électrique. Elle est chargée de stocker les données intermédiaires ou les résultats de calculs. On peut lire ou écrire des données dedans.

Il existe une hiérarchie des mémoires informatiques : les plus rapides sont les plus coûteuses, donc en nombre limité, et placées le plus près du processeur. Les plus lentes sont les moins coûteuses et sont éloignées du processeur.[^2] 

![Hierarchie de la mémoire](./fig/hierarchiememoire.png)  

> 📢Un __registre__ est une mémoire interne (_faisant partie intégrante du processeur_) très rapide 
  
### Unités de mesure  

Les unités de mesure des capacités de ces mémoires sont le plus souvent exprimé en multiples d'octet. Historiquement les multiples utilisés en informatique étaient des puissances de 2 : ceci peut engendrer des confusions

| Unité standard | Variante binaire|  
|:---:|:---:|   
| kilooctet 1 ko = $10^{3}o$| kibiooctet 1Kio = $2^{10}o=1024o$|  
| mégaoctet 1 Mo = $10^{6}o$| mébiooctet 1Mio = $2^{20}o=1048576o$|  
| giagaoctet 1 Go = $10^{9}o$| gibiooctet 1Kio = $2^{30}o=1073741824o$|  

## L'unité centrale CPU (Central Processing Unit)

Le CPU (ou plus simplement processeur) regroupe l’__unité de contrôle__ et l’__unité arithmétique et logique__.
L'unité de contrôle (UC) joue le rôle du "chef d'orchestre" : elle lit en mémoire un programme et le transmet à l'UAL qui exécute une séquence d'instructions.

Le processeur est  cadencé au rythme d’une __horloge interne__.
On caractérise le processeur par :

* sa `fréquence d'horloge` exprimée en __Hertz__ (Hz) traduisant le nombre de cycles par secondes   
* le nombre d’instructions par secondes qu’il est capable d’exécuter : en __MIPS__ (millions d'instructions par seconde)    
* la taille des données qu’il est capable de traiter : en __bits__  

### L'unité de contrôle (ou _unité de commande_)

L' __UC__  est un circuit logique séquentiel (on l'appelle aussi _séquenceur_) contrôlant tous les mouvements de données de la mémoire vers l'UAL ou les périphériques d'entrées-sorties.

Il contient notamment un registre spécial appelé __compteur de programme__

> 📢Le __compteur de programme ou PC__  est un registre contenant l'adresse de l'instruction courante (_en fonction des architectures il peut aussi pointer vers la prochaine instruction à exécuter_)

  
### L'unité arithmétique et logique

> 📢 L'__UAL__ est une unité de traitement composée :  
> 
> * de plusieurs registres de données
> * de circuits électroniques réalisant les opérations arithmétiques, logiques, de comparaisons etc...  
> * d'un registre spécial appelé __accumulateur__ dans lequel sont versés les résultats des calculs effectués par l'UAL

![Schéma de l'unité arithmétique et logique](./fig/ual.jpg)

## Les dispositifs d'entrées-sortie

* Les entrées sont les données envoyées par un périphérique (disque, réseau, clavier, souris, capteur…) à destination d'une unité centrale de traitement (processeur) ;
* Les sorties sont les données émises par une unité centrale de traitement à destination d'un périphérique (disque, réseau, écran, imprimante, actionneur…).

Il sont désignés par l'acronyme __I/O__ (pour _Input/Output_) ou encore E/S

> 📢 Les périphériques d'entrées-sortie sont connectés au reste du dispositif via des circuits électroniques appelés __ports__.  
 

## Qu’en est-il aujourd’hui ?

Plus de soixante ans après son invention, le modèle d’architecture de von Neumann régit toujours l’architecture des ordinateurs. Par rapport au schéma initial, on peut noter deux évolutions.

![modèle actuel](./fig/modele-actuel.gif)

Les entrées-sorties, initialement commandées par l’unité centrale, sont depuis le début des années 1960 sous le contrôle de processeurs autonomes . 
   
Les ordinateurs comportent maintenant des **processeurs multiples**, qu’il s’agisse d’unités séparées ou de « cœurs » multiples à l’intérieur d’une même puce. Cette organisation permet d’atteindre une puissance globale de calcul élevée sans augmenter la vitesse des processeurs individuels, limitée par les capacités d’évacuation de la chaleur dans des circuits de plus en plus denses.


# Programmer le processeur 

## Le langage assembleur  

![chaîne de compilation](./fig/compilation.jpg)

Un programme écrit dans un __langage de haut niveau__ (comme Python par exemple) est proche du langage naturel: il dépend le moins possible de la nature du processeur et du système d'exploitation. Ces programmes seront toujours traduits dans un __langage de plus bas niveau__  avant d'être exécuté par le microprocesseur dans un langage machine (en binaire). 

Chaque processeur posséde un __langage assembleur__ : par exemple la documentation relative au INTEL core I5 regroupe 2,5 millions de mots permettant de réaliser différentes instructions. Il est parfois nécessaire d'écrire des "morceaux" de programmes directement en langage assembleur quand par exemple un caclul précis doit être réalisé le plus vite possible.

## La machine débranchée M999

[Machine débranchée M999a](./M999a.pdf) : _Voir Annexe_

La machine M999 est une architecture qui comporte 100 mots mémoires de 3 chiffres emplacements mémoire adressables par des adresses codées sur 2 chiffres. Cette mémoire va contenir données et instructions.  
Deux registres A et B  forment l'entrée de l'UAL.  
Un accumulateur R reçoit le résultat de l'UAL.  
Le processeur dispose aussi d'un registre compteur de programme __PC__ contenant l'adresse mémoire de la prochaine instruction à exécuter.  


###  Jeu d'instructions 

Pour écrire un programme en langage assembleur, on utilise des mots appelés __mnémoniques__. 

* [Jeu d'instructions M999 et Registres](./jeu_instructions_registres_m999a.pdf) : _Voir Annexe_  

Pour comprendre comment on code en langage assembleur, nous allons commenter certaines instructions :   

- **LDA 12** se traduira d'après le tableau par **012** (0 est le code attribué à LDA) ce qui veut dire charger la valeur contenue dans l'adresse mémoire 12 dans le registre A.  

- **JMP 14** se traduira par **514**. Cela consiste à réaliser un saut de l'adresse où l'on se trouve jusqu'à l'adresse 14.

- On suppose que le registre A contient la valeur 5 et le registre B la valeur 8. L'instruction **ADD** se traduira par **300**. Cela va réaliser l'addition du contenu de A et B et le mettre dans R. Donc R contiendra la valeur 13.

### Un exemple de fonctionnement :  [Diaporama commenté](./exemple_utilisation_m999/exemple_m999.pdf)

[^1]: Par Mutatis mutandis — Travail personnel, CC BY 2.5, https://commons.wikimedia.org/w/index.php?curid=1406914
[^2]: https://cache.media.eduscol.education.fr/file/NSI/76/7/RA_Lycee_G_NSI_arch_systemes_approfondis_1170767.pdf


Sources :  
 
* https://interstices.info/le-modele-darchitecture-de-von-neumann/
* Numérique et sciences informatiques (Balabonski, Conchon, Filiâtre, Nguyen)
* M999, le processeur débranché, Martin Quinson, Philippe Marquet
github.com/InfoSansOrdi/M999


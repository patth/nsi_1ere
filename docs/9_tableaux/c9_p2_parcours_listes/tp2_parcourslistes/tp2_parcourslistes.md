---
title: "Chapitre 9 : Une structure de donnée construite, les tableaux "
subtitle: "TP2 : Parcours de listes"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---  

# Exercice 1 : QCM   &#x1F3C6; 

__1)__ Quel est le contenu de `liste` après ces instructions?      

```python
liste = [0]
for _ in range(3):
    liste = liste + [1]
```  

* [ ] `[0, 0, 0]`
* [ ] `[0, 1, 1, 1]`
* [ ] `[1, 1, 1, 1]`
* [ ] `[1, 3, 1]` 


__2)__  Quel est le contenu de `liste` après ces instructions?     

```python
liste = []
for i in range(4):
    liste.append(2 * i)
```  

* [ ] `[0, 1, 2, 3]`
* [ ] `[0, 2, 4, 6]`
* [ ] `[0, 2, 4, 6, 8]`
* [ ] `[1, 2, 4, 6]`


__3)__  Quel est le contenu de `liste` après ces instructions?     

```python
liste = [1, 2, 3, 4, 5]
for _ in range(2):
    liste.pop()
```  

* [ ] `[1, 2, 3]` 
* [ ] `[3, 4, 5]`
* [ ] `[1, 3, 4, 5]`
* [ ] `[1, 2, 4, 5]`


__4)__  Quel est le contenu de `liste` après ces instructions?     

```python
nombres = [1, 2, 3, 4, 5]
liste = []
for n in nombres :
    if n % 2 == 0 :
        liste.append(n)
```  

* [ ] `[1, 3, 5]` 
* [ ] `[2, 4]`
* [ ] `[0.5, 1.0, 1.5, 2.0, 2.5]`
* [ ] `[0, 0, 0, 0, 0]`


# Exercice 2 : Créations de listes  &#x1F3C6; &#x1F3C6; 

Définir trois fonctions nommées `creer_liste`, `creer_liste_bis` et `creer_liste_ter` avec un paramètre `n` (_de type entier_). Chacune de ces fonctions renverra une liste contenant par ordre croissant les n premiers entiers naturels mais la conception sera différente :

* Pour `creer_liste` vous utiliserez une boucle `for` et la méthode `append`
* Pour `creer_liste_bis` vous utiliserez une boucle `for` et la concaténation
* 🥇 Pour `creer_liste_ter` vous utiliserez une boucle `for` mais ni de concaténation et ni la méthode `append`


# Exercice 3 : Parcours de listes  &#x1F3C6; &#x1F3C6; 

__1)__ Définir une fonction `produit` acceptant comme paramètre une liste de nombres. Elle renverra le produit de tous ces nombres. Par exemple :

```python
>>> produit([2, 3, 4])
 24
```

__2)__ Définir une focntion `nombre_occurences` acceptant comme paramètre une liste de nombres et un nombre. Elle renverra le nombre d'occurences de ce nombre dabs la liste. Par exemple :

```python
>>> nombre_occurences([1, 1, 1, 5, 8], 1)
 3
```

__3)__ 🥇 Définir une fonction `lancer_mille_des` sans paramètre.

Cette fonction générera tout d'abord une liste aléatoire de mille nombres entiers compris entre 1 et 6. Elle affichera le nombre d'occurences de chaque valeur dans la liste sous la forme suivante :

```python
>>> lancer_mille_des()
 Dé  1  :  142 fois
 Dé  2  :  175 fois
 Dé  3  :  174 fois
 Dé  4  :  151 fois
 Dé  5  :  188 fois
 Dé  6  :  171 fois 
```

Vous utiliserez la fonction  `nombre_occurences` précédemment créée pour simplifier votre code.


# Exercice 4 : Le bulletin  &#x1F3C6; &#x1F3C6; 

__1)__ Créer un script nommé `bulletin.py` et définissez tout d'abord une variable globale `notes` qui sera pour l'instant une liste vide.    

__2)__ Définissez une fonction `entrer_notes` sans paramètre.  
Cette fonction doit permettre de demander à l'utilisateur de rentrer des notes qui seront ajoutées à la liste `notes`. 
Pour cela, on demandera à chaque fois si l'utilisateur veut entrer une note :

* Si la réponse est __oui__ on demande la note, on la rajoute à `notes` et on repose la question
* si la réponse est __non__ on ne fait plus rien.
* Si la réponse est différente de __oui ou non__ on repose la question après avoir précisé que l'entrée était mauvaise.  

Cette fonction n'a pas de valeur de retour elle modifie `notes` par effet de bord

Testez votre fonction, comme ceci par exemple :

```python
>>> entrer_notes()
 Voulez-vous entrer une note? (répondez par oui ou non)  oui
 Quelle est la note? 14
 Voulez-vous entrer une note? (répondez par oui ou non)  non

>>> notes
[14]
```

__3)__ Définissez une fonction __calcul_moyenne__ acceptant comme paramètre une liste `notes` et renvoyant la moyenne des éléments de la liste.  

```python
>>> calcul_moyenne([10, 15, 8])
 11.0
```

__3)__ Définissez une fonction __recherche_max__ acceptant comme paramètre une liste `notes` et renvoyant la plus grande note de la liste

```python
>>> recherche_max([10, 15, 8])
 15
```

__4)__ Définissez une fonction __recherche_min__ acceptant comme paramètre une liste `notes` et renvoyant la plus petite note de la liste

```python
>>> recherche_min([8, 10, 15, 8])
 8
```

__5)__ Définissez une procédure `affiche_notes(notes)` acceptant un paramètre `notes` : une liste de notes
Cette procédure réalisera un affichage sur le modèle suivant

```python
>>> affiche_notes([10, 14, 8])
 Voici les notes obtenues : 10-14-8-
```

# Exercice 5 : Cryptographie  le code de César &#x1F3C6; &#x1F3C6;&#x1F3C6;  

C'est une méthode ancienne de cryptographie qui consiste à réaliser un décalage constant dans l'ordre alphabétique. Ce mode de cryptographie a été rapidement abandonné car une fois qu'on connait la méthode, le décryptage est très simple. [^1]

![chiffrement par decalage](./fig/caesar.png)

__1)__ Créer un script nommé `cesar.py` et définissez tout d'abord une variable globale `alphabet` ainsi :  

```python
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 
            'g', 'h', 'i', 'j', 'k', 'l', 
            'm', 'n', 'o', 'p', 'q', 'r', 
            's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
```

__2)__ 🥇 Définissez une fonction `decaler_alphabet`  qui prendra en paramètre un entier `n` qui représente le décalage à effectuer.
   
La fonction renverra une nouvelle liste dont les lettres auront subi un décalage de n positions par rapport à la liste globale `alphabet`

``` python
>>> liste_decalee = decaler_alphabet(2)
>>> liste_decalee
 ['y', 'z', 'a', 'b', 'c', ...]
```

* Vous créerez tout d'abord dans la fonction une copie indépendante de `alphabet`
* On pourra utiliser ensuite les méthodes `pop` et `insert` pour compléter cette nouvelle liste à partit de `alphabet`

|syntaxe| description|  
|:---:|:---:|  
|`liste.pop()`| renvoie le dernier élément de liste et le supprime de liste|    
|`liste.insert(i, x)`| insère x dans liste à l'index donné par i|  

__Le principe est donc d'enlever le dernier élément de la liste avec `pop` puis de l'insérer au début avec `insert` et ceci __n__ fois (n étant le décalage).__

__3)__ Créer une fonction `crypter_lettre` qui prendra deux paramètres :  

* `lettre` : la lettre à crypter
* `liste_decalee` : la liste décalée à partir de alphabet

La valeur de retour sera la nouvelle lettre cryptée correspondant au caractère passé en paramètre.   
Pour y arriver il faut trouver l'indice de `lettre` dans la varaible globale `alphabet` renvoyer la lettre contenue au même indice dans `liste_decalee`

_AIDE_ : On pourra utiliser la méthode `index`

|syntaxe| description|
|:---:|:---:|
|`liste.index(x)`| renvoie l'indice de la première occurence de x dans liste|

\newpage

__4)__ Définissez la fonction principale `crypter_mot` à partir de cet algorithme :

```
Entrée : aucune
Sortie : aucune

1:  cle ← Demander le décalage souhaité
2:  liste_decalee ← Décaler l'alphabet du décalage souhaité
3:  mot ← Demander le mot à crypter
4:  mot_crypte ← ''          // chaîne de caratères vide
4:  POUR chaque lettre dans mot faire :
5:     ajouter lettre cryptée à mot_crypte 
6:  fin pour
7:  Afficher mot_crypte
```

_Remarque_ : Attention l'alphabet utilsié nous empêche d'utiliser des lettres capitales dans le mot à crypter !  
C'est modifiable bien évidemment.

[^1]: https://fr.wikipedia.org/wiki/Chiffrement_par_d%C3%A9calage


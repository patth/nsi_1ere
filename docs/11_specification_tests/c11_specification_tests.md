---
title: "Chapitre 11 : Spécification et tests des programmes "
subtitle: "Cours"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

La __spécification__  d'une fonction consiste à décrire de manière explicite ce que doit faire cette fonction. 
Nous allons voir comment spécifier et tester son programme pour être le plus rigoureux possible.

# Spécification des fonctions

## Documenter son programme

Il est fortement conseillé de donner une documentation (description) à chaque fonction c'est ce que nous avons déjà fait à l'aide de __docstrings__.

```python
def division_euclidienne(a, b) :
    """ 
    Renvoie le quotient et le reste de la division euclidienne de a par b
    """
    quotient = a // b  
    reste = a % b  
    return [quotient, reste] 
```

La documentation pour être réellement efficace doit cependant contenir plus d'informations : ici par exemple l'utilisation de cette fonction sous-entend que a et b sont des entiers.  
Il est possible d'améliore la docstring de cette manière :

```python
def division_euclidienne(a, b) :
    """ Renvoie le quotient et le reste d'une division euclidienne

    :param a: diviseur
    :type a: int
    :param b: dividende
    :type b : int
    :return: [quotient, reste] 
    :rtype: list[int]
    """

    # suite de la fonction
```

> Les informations peuvent être structurées en utilisant un format appelé __ReStructuredText (reST)__.
> 
> * chaque paramètre associé est décrit ainsi   
> `:param <nom_du_parametre>: <description>`
> * le type de données est aussi explicité pour chacun d'eux   
> `:type <nom_du_parametre>: <type de données>`
> * la valeur de retour est aussi décrite   
>  `:return: <description>` 
> * ainsi que son type  
>  `:rtype:`  

IL est aussi possible de synthétiser le type et le ne nom des paramètres en une seule ligne.

```python
def division_euclidienne(a, b) :
    """ Renvoie le quotient et le reste d'une division euclidienne

    :param int a: diviseur
    :param int b: dividende
    :return: [quotient, reste] 
    :rtype: list[int]
    """

    # suite de la fonction
```

> La syntaxe compacte pour décrire un parametre et son type est   
> `:param <type> <nom>: <description>`

## Préconditions

> 📢 Les `préconditions ` doivent garantir que l'exécution du traitement est possible sans erreur.   

Toujours en reprenant le même exemple que précédemment, le fait de décrire que `a` et `b` soient entiers ne suffit pas à garantir le bon fonctionnement. On peut lister plusieurs __préconditions__ : 

* `a` et `b` sont deux entiers
* $a \geq 0$ et  $b > 0$

> 📢 Il est possible de faire apparaitre ces préconditions dans les docstrings à l'aide d'une rubrique __Contraintes d'utilisation__ `:CU:` pour spécifier encore davantage la fonction.

Par exemple :

```python
def division_euclidienne(a, b) :
    """ Renvoie le quotient et le reste d'une division euclidienne

    :param int a: diviseur
    :param int b: dividende
    :return: [quotient, reste] 
    :rtype: list[int]

    :CU: a > 0 and b >= 0 
    """

    # suite de la fonction
```

# Programmation défensive

La documentation ne protége pas d'une mauvaise utilisation d'une fonction surtout si on ne la lit pas!  Si on souhaite éviter des erreurs ou des résultats incohérents on peut interrompre le programme dès lors que la fonction reçoit une information non valide ou renvoyer des valeurs spéciales : c'est ce qu'on appelle la __programmation défensive__

## Assertions

> 📢 Une __assertion__ est une expression qui doit être évaluée à vrai. Si cette évaluation échoue elle peut mettre fin à l'exécution du programme.   
> Elle a pour syntaxe : `assert condition , "message"`

* _Par exemple :_

```python
>>> a = -3
>>> assert a > 0, "a doit être positif"
 Traceback (most recent call last):
   File "<pyshell>", line 1, in <module>
 AssertionError: a doit être positif
```

Ici l'assertion `a > 0` n'est pas vraie, le programme s'interrompt et renvoie le message d'erreur associé que nous avons préalablement choisi `"a doit être positif"`.

* Dans le cas de la fonction `division_euclidienne` on pourrait par exemple s'assurer que le dividende est entier mais aussi strictement positif avant de calculer quotient et reste. En fait on va vérifier un ou plusieurs préconditions avant l'exécution du code de la fonction

```python
def division_euclidienne(a, b) :
    """ Renvoie le quotient et le reste d'une division euclidienne

    :param int a: diviseur
    :param int b: dividende
    :return: [quotient, reste] 
    :rtype: list[int]

    :CU: a > 0 and b >= 0 
    """
    assert type(b) == int  , "le dividende doit être entier"
    assert b != 0 , "le dividende ne peut être nul"

    quotient = a // b  
    reste = a % b  
    return [quotient, reste] 
```

Réalisons maintenant un appel ne vérifiant pas la première précondition :

```python
>>> division_euclidienne(5, 1.5)

 Traceback (most recent call last):
   File "<pyshell>", line 1, in <module>
   File "C:\Users\Desktop\test.py", line 7, in division_euclidienne
     assert type(b) == int  , "le dividende doit être entier"
 AssertionError: le dividende doit être entier
```

La première assertion n'est pas vraie : le programme est interrompu et un message d'erreur permet d'expliciter ce qui pose problème. 

> L'utilisation du mot clé `assert` en Python permet de réaliser des tests unitaires.

## La valeur spéciale  None

Une autre façon d'être défensif consiste, plutôt que d'échouer, à renvoyer une valeur qui ne peut être confondue avec un résultat valide.

```python
def division_euclidienne(a, b) :
    # docstring

    if b == 0:
        return None

    # suite des instructions 
```

```python
>>> division_euclidienne(5, 0)
>>> 
```

> La valeur spéciale `None` est utilisée pour représenter l'absence de valeur.     
> C'est la même qui est est renvoyée par les procédures (comme les affichages) : son type est `NoneType`  

```python
>>> type(print())
 <class 'NoneType'>
```

# Vérifier la validité de vos programmes

Spécifier sa fonction et vérifier les préconditions peuvent ne pas suffir à garantir la validité de votre programme. En effet ce n'est pas parce que votre programme vous renvoie une valeur que c'est la bonne !

## Postconditions

> 📢 Les `postconditions` doivent garantir que le traitement a bien réalisé son travail.  

Toujours dans le cas de la fonction `division_euclidienne` une postcondition est par exemple: $a = quotient \times b + reste$.

On pourrait tout à fait vérifier à l'aide d'assertions les postconditions dans une fonctions mais attention à ne pas surcharger votre code.  
_Remarque_ : les invariants de boucle sont des exemples de postconditions pour valider la correction d'une boucle.


## Le module doctest

# Présentation

Le module `doctest` permet d’inclure les tests dans la docstring descriptive de la fonction écrite. On présente dans la docstring, en plus des explications d’usage, des examples d’utilisation de la fonction tels qu’ils pourraient être tapés directement dans le Shell.

```python
def division_euclidienne(a, b) :
    """ Renvoie le quotient et le reste d'une division euclidienne

    :param int a: diviseur
    :param int b: dividende
    :return: [quotient, reste] 
    :rtype: list[int]   

    :CU: a >0 and b >= 0 

    :example:
    >>> division_euclidienne(5, 2)
    [2, 1]

    >>> division_euclidienne(11, 3)
    [3, 2]
    """
    # suite des instructions
```

> 📢 Les doctests doivent être placés dans la rubrique `:example:` de la docstring.

Afin d'exécuter l'ensemble des tests on peut utiliser dans le Shell les instructions suivantes :

```python
>>> import doctest
>>> doctest.testmod()

TestResults(failed=0, attempted=2)
```

La fonction `testmod` du module `doctest` est allée chercher dans les docstring des fonctions du module actuellement chargé tous les exemples (reconnaissables à la présence des triples chevrons `>>>`), et a vérifié que la fonction documentée satisfait bien ces exemples. Dans le cas présent, une seule fonction dont la documentation contient deux exemples (attempted=2) a été testée, et il n’y a eu aucun échec (failed=0).

Si les résultats annoncés dans le doctest ne correspondent pas à ceux qui sont renvoyés par la console, chacun des tests est détaillé

__📝Application__ :

Faussons volontairement le premier test  le résultat du premier exemple dans la docstring puis relançons la vérification :

```python
    """
    >>> division_euclidienne(5, 2)
    [4, 1]
    """
```

```python
.......
File "test.py", line 12, in __main__.division_euclidienne
Failed example:
    division_euclidienne(5, 2)
Expected:
    [4, 1]
Got:
    [2, 1]
.........
**********************************************************************
1 items had failures:
   1 of   2 in __main__.division_euclidienne
2 tests in 2 items.
1 passed and 1 failed.
***Test Failed*** 1 failures.
```

## Automatiser les doctests dans votre script

Le nombre d'espace dans les tests a une influence puisque le module `doctest` vérifie la syntaxe exacte fournie par la docstring. Afin d'éviter ce problème et lancer votre jeu de tests dés l'exécution du script ajoutez les lignes suivantes __à la fin du script__:

```python
if __name__ == '__main__':
    import doctest
    doctest.testmod(optionflags=doctest.NORMALIZE_WHITESPACE | doctest.ELLIPSIS, verbose=False)
```

Vous remarquerez également qu'aucun message n'est affiché si tous les tests passent avec succés. (`verbose = False`)

## Bon ensemble de tests

Même si l’écriture de tests ne garantit en rien qu’un programme est correct, le fait de réfléchir aux cas limites que l’on peut rencontrer et en écrire une vérification explicite permet de délimiter correctement le problème.
Il est dificile de déterminer si l'ensemble de tests est suffisant mais on peut déjà se baser sur les points suivants :

* Tester les __cas particuliers__ évoqués dans la description des paramètres. 
* Si une fonction renvoie un __booléen prévoir au moins deux tests__ avec des résultats différents.
* Si une fonction utilise un __nombre__, prévoir des tests pour des __valeurs positives, négatives et nulle__.
* Si une fonction fait intervenir des valeurs appartenant à un intervalle, il est utile de prévoir des __tests aux limites de cet intervalle__.






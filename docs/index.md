# Documents pour la spécialité 1ère NSI

_[Patrice Thibaud](mailto::patrice.thibaud@univ-lille.fr)  : professeur de NSI au lycée Marguerite de Flandre (59147 Gondecourt) en 2019-2020_

# 📜 Contributions et licence

* La plupart des documents proposés sur ce site sont sous licence creative commons : __BY-NC-SA__.  
Ce sont pour la plupart des document originaux que j'ai conçu et pour certains par l'équipe pédagogique de NSI du [lycée Marguerite de Flandre](https://marguerite-de-flandre-gondecourt.enthdf.fr/) et notamment [André Mattot](mailto::andre.mattot@ac-lille.fr) avec qui j'ai travaillé en étroite collaboration.

![licence](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

_Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre_  
Merci de __respecter les conditions de la__ [__licence__](https://creativecommons.org/licenses/by-nc-sa/2.0/fr/)

* Pour les documents provenants de sources extérieures : ces sources sont sytématiquement citées

# Site statique associé

Vous pouvez également consulter le site statique généré à l'aide de _mkdocs_   
https://patth.frama.io/nsi1ere/


# Contact

Pour toute demande :
[📧](mailto::patrice.thibaud@univ-lille.fr)
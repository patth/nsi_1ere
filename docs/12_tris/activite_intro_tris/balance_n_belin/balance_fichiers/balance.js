"use strict";

var LARGEUR = 100; // Largeur et hauteur des pierres en pixels
var N = 10; // Nombre d'éléments à classer
var tri = {};

function $(e) {
    return document.getElementById(e);
}

tri.init = function() {
    var i, pierre, recep;
    
    $('table').style.height = 4 * LARGEUR + 'px';
    
    this.listePierres = [];
    this.listeReceptacles = [];
    this.nombrePesees = 0;
    
    for(i = 0; i < N; i++) {
        pierre = new Pierre(i);
        pierre.position(20 + i * (LARGEUR+20), 20)
        pierre.poids = Math.random()
        this.listePierres.push(pierre);

        recep = new Receptacle(i);
        this.listeReceptacles.push(recep);
    }

    $('plateauG').style.width = LARGEUR + 'px';
    $('plateauG').style.height = LARGEUR + 'px';
    $('plateauD').style.width = LARGEUR + 'px';
    $('plateauD').style.height = LARGEUR + 'px';

    this.plateauGPoids = undefined;
    this.plateauDPoids = undefined;
};

function dragstart(ev) {
    var rect;

    ev.dataTransfer.setData("text", ev.target.id);
    var pierre = tri.listePierres[parseInt(ev.target.id)];
    pierre.clientX = ev.clientX;
    pierre.clientY = ev.clientY;
    if( ! pierre.surTable) {
        rect = pierre.element.getBoundingClientRect();
        pierre.left = rect.left;
        pierre.top = rect.top;
    }
    if(pierre.surPlateauG) {
        $('plateauG').style.top = "0px";
        $('plateauD').style.top = "0px";
        tri.plateauGPoids = undefined;
        pierre.surPlateauG = false;
    }
    else if(pierre.surPlateauD) {
        $('plateauG').style.top = "0px";
        $('plateauD').style.top = "0px";
        tri.plateauDPoids = undefined;
        pierre.surPlateauD = false;
    }
    else if(pierre.surRecepIndex != undefined) {
        tri.listeReceptacles[pierre.surRecepIndex].poids = undefined;
        pierre.surRecepIndex = undefined;
    }
}

function dropTable(ev) {
    ev.preventDefault();
    var id = ev.dataTransfer.getData("text");
    var pierre = tri.listePierres[parseInt(id)];
    if( ! pierre.surTable) {
        ev.target.appendChild(pierre.element);
        pierre.surTable = true;
        var rect = ev.target.getBoundingClientRect();
        pierre.clientX += rect.left;
        pierre.clientY += rect.top;
    }
    pierre.position(pierre.left + ev.clientX - pierre.clientX,
                    pierre.top + ev.clientY - pierre.clientY);
}

function dropPlateau(ev) {
    ev.preventDefault();
    var id = ev.dataTransfer.getData("text");
    var pierre = tri.listePierres[parseInt(id)];
    pierre.surTable = false;
    pierre.position(13, 13)
    ev.target.appendChild(pierre.element);
    if(ev.target.id == "plateauG") {
        tri.plateauGPoids = pierre.poids;
        pierre.surPlateauG = true;
    }
    else {
        tri.plateauDPoids = pierre.poids;
        pierre.surPlateauD = true;
    }
    if(tri.plateauGPoids != undefined && tri.plateauDPoids != undefined) {
        tri.nombrePesees++;
        $('nombre_pesees').textContent = tri.nombrePesees.toString();
        if(tri.plateauGPoids >= tri.plateauDPoids) {
            $('plateauG').style.top = "52px";
            $('plateauD').style.top = "-40px";
        }
        else {
            $('plateauG').style.top = "-40px";
            $('plateauD').style.top = "52px";
        }
    }
}

function dropRecep(ev) {
    var i, p0, p1;

    ev.preventDefault();
    var id = ev.dataTransfer.getData("text");
    var pierre = tri.listePierres[parseInt(id)];
    pierre.surTable = false;
    var recepIndex = parseInt(ev.target.id.substring(1));
    pierre.surRecepIndex = recepIndex
    var receptacle = tri.listeReceptacles[recepIndex]
    receptacle.poids = pierre.poids
    ev.target.appendChild(pierre.element);
    pierre.position(0, 0)
    for(i=0; i<N-1; i++) {
        p1 = tri.listeReceptacles[i+1].poids;
        p0 = tri.listeReceptacles[i].poids;
        if(p1 == undefined || p0 == undefined || p1 < p0) {
            break;
        }
    }
    if(i == N-1) {
        $('reussite').style.display = 'inline';
    }
}

function allowDrop(ev) {
    ev.preventDefault();
}

function Pierre(index) {

    var id = '' + index;
    this.id = id;
    this.element = $('pierreTemplate').cloneNode(false);
    this.element.title = id;
    this.element.id = id;
    this.element.style.width = LARGEUR + 'px';
    this.element.style.height = LARGEUR + 'px';
    this.element.style.backgroundColor = this.getColorString()
    $('table').appendChild(this.element);
    this.surTable = true;
    this.surPlateauG = false;
    this.surPlateauD = false;
    this.surRecepIndex = undefined
    //this.element.addEventListener('mousedown', this, true);
}

Pierre.prototype.getColorString = function() {
    return 'rgb(' + Math.round(Math.random() * 255.0) + ',' +
        Math.round(Math.random() * 255.0) + ',' +
        Math.round(Math.random() * 255.0) + ')'; 
};

Pierre.prototype.position = function(x, y) {
    this.element.style.left = x + 'px';
    this.element.style.top = y + 'px';
    this.left = x
    this.top = y
};

Pierre.prototype.handleEvent = function(event) {
    switch(event.type) {
    case 'mousedown':
        break;
    }
};

function Receptacle(index) {

    var id = 'r' + index;
    this.element = $('receptacleTemplate').cloneNode(false);
    this.element.title = id;
    this.element.id = id;
    this.element.style.width = LARGEUR + 'px';
    this.element.style.height = LARGEUR + 'px';
    $('receptacles').appendChild(this.element);
    this.poids = undefined
}

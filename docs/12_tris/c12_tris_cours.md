---
title: "Chapitre 12 : Les tris"
subtitle : "Cours : Tris par insertion et sélection"
papersize: a4
geometry: margin=1.5cm
fontsize: 12pt
lang: fr
---

Les algorithmes de tris sont essentiels dans le traitement des données.  
Il est en effet bien plus simple de rechercher un livre dans une bibliothéque déjà triée comme un mot dans un dictionnaire ou encore une valeur dans un tableau.  
Les algorithmes de tris sont nombreux, nous nous intéresseros principalement à deux algorithmes simples : le __tri par insertion__ et le __tri par sélection__.

# Le tri par insertion

## Principe

Le tri par insertion s'inspire de la manière dont les gens tiennent des cartes à jouer.  
Au début la main gauche du joueur est vide, il prend les cartes une par une dans sa main.  
Pour savoir où placer une carte dans son jeu, il la compare avec chacune des cartes déjà présentes dans sa main en les examinant de droite à gauche.  
A tout moment les cartes tenues par la main gauche sont triées.[^1] 

![tri joueur de cartes](./fig/cartes.jpg)

Une animation pour visualiser le tri par insertion : http://lwh.free.fr/pages/algo/tri/tri_insertion.html

## Pseudo-code

On considère un tableau de nombres `T` contenant `n` élèments.  
On souhaite le trier par ordre croissant des nombres. 


```
Entrée : T, un tableau de n nombres (d'indices allant de 0 à n-1)
Effet de bord : le tableau est trié en place par ordre croissant

1:  n ←  longueur de T
2:  POUR i allant de 1 à n-1 faire :
3:      x ← T[i]        
4:      j ← i            
5:      TANT QUE  j > 0 et T[j-1] > x faire :
6:          T[j] ← T[j - 1]
7:          j ← j-1
8:      fin TANT QUE
9:      T[j] ← x
10: fin POUR
```

__Analyse du pseudo-code :__

* On parcourt le tableau de _i = 1_ à jusqu'au dernier élément( _i= n-1_ ).  
* A chaque étape _i_, on considère que les éléments de _0 à i-1_ du tableau sont déjà triés.  
* On va alors placer le $i^{ème}$ élément à sa bonne place parmi les élèments précédents du tableau, en le faisant "redescendre" jusqu'à atteindre un élément qui lui est inférieur  

> 📢 Le tri par insertion est un __tri en place__ : il modifie directement le tableau qu'il est en train de trier

## Un exemple

Voici les étapes de l'exécution du tri par insertion sur le tableau `[6, 5, 3, 1, 8, 7, 2, 4]`. Le tableau est représenté au début et à la fin de chaque itération.[^2]

![Exemple de déroulé pour le tri par insertion](./fig/deroule_insertion.jpg)

## Coût temporel

Le temps d'exécution du tri par insertion est fortement dépendant de l'ordre du tableau initial. 
   
* Dans le __pire des cas__ (celui où les entiers sont rangés par ordre décroissant) : il faudra réaliser des échanges à chaque comparaison. Il y a $`(n-1)`$ itérations de la boucle POUR :
  
  * A l'étape 1 il y a 1 comparaison : on compare T[1] et T[0]
  * A l'étape 2, il y a 2 comparaisons : on compare T[1] avec T[2] puis T[0] avec T[2]
  * etc...
  * A l'étape n-1,  il y a n-1 comparaisons  à effectuer

Au total il y aura $1+2+\dots + (n-2) + (n-1)$ comparaisons.
On peut montrer que cette somme est égale à  $n^{2} - \dfrac{n}{2}$.  le nombre d'échanges varie comme $n^{2}$.   
Le temps d'exécution maximal varie donc comme le carré du nombre de données à traiter.

* Dans le __meilleur des cas__ si le tableau initial est trié, il n'y aura qu'une comparaison à effectuer à chaque étape soit __n-1__ comparaisons à effectuer au total.
Le temps d'exécution minimal varie donc comme le nombre de données à traiter.

> 📢 On retiendra que l'algorithme de tri par insertion a __un coût temporel quadratique an moyenne__ (_e.g. le nombre d'opérations nécessaires évolue proportionnellement avec le carré du nombre de données à traiter_)  
> Son exécution dépend fortement de la nature du tableau à trier (_plus rapide pour un tableau déjà partiellement trié_)    

## Preuve de la terminaison

La boucle POUR est une boucle bornée, elle se termine nécessairement.  
La boucle TANT QUE accepte a pour variant $j$ :

* En effet au début de la boucle $j = i$ et $i >= 1$ donc j est un entier positif
* A chaque tour de boucle j décroit d'une unité il deviendra donc égal à 0 et la condition boucle ne sera plus vraie : elle se terminera.

> _Remarque_ : on aurait pu écrire l'algorithme uniquement avec des boucles tant que. Il aurait alors fallu trouver des variants pour chacune d'entre elles.   
> Pour être tout à fait rigoureux il est important de préciser que dans le cas de la boucle POUR on ne modifie:
> *  ni la variable de boucle dans le bloc d'instructions correspondant 
> * ni la longueur de la liste

## Preuve de la correction

> 📢 Un invariant de boucle pour le tri par insertion est défini par la propriété suivante : __"La liste `t[0:i]` est triée"__

- __INITIALISATION__ : Avant d'entrer dans la boucle TANT QUE, i = 1 : la liste `t[0:1]` est trié puisqu'il ne contient qu'un seul élément `t[0]`
- __CONSERVATION__ : Si au début de  l'étape i la liste `t[0:i]` _(de l'indice 0 à l'indice i exclus)_  est triée alors la liste `t[0:i+1]` _(de l'indice 0 à l'indice i inclus)_ est triée à la fin de l'étape puisque la valeur `t[i]` aura été correctement insérée.
- __TERMINAISON__ : A la fin de la boucle extérieure `i = n-1` et la tranche `t[0:n]` (autrement dit la liste complète) est triée

L'algorithme est donc correct.

# Le tri par sélection

## Principe

Le principe du tri par sélection est d'aller chercher le plus petit élément du tableau pour le mettre en premier, puis de repartir du second élément et d'aller chercher le plus petit élément du sous-tableau restant pour le mettre en second, etc.... 

Une animation pour visualiser le tri par selection : http://lwh.free.fr/pages/algo/tri/tri_selection.html

## Pseudo -code

On considère toujours tableau de nombres à trier par ordre croissant des nombres.  

```
Entrée : T, un tableau de n nombres (d'indices allant de 0 à n-1)
Effet de bord : le tableau est trié en place par ordre croissant

1:  n ←  longueur de T
2:  POUR i allant de 0 à n - 2 faire :
3:      m ← i            
4:      POUR j allant de i + 1 à n - 1 faire:           
5:          SI T[j] < T[m] alors :
6:              m ← j
7:      fin POUR
8:      SI T[i] différent T[m] alors :
9:          échanger T[i] et T[m]
10:     fin SI
10: fin POUR
```

__Analyse du pseudo-code pour un tableau de taille n :__ 

* On parcourt le tableau du début (_i= 0_) jusqu'à l'avant dernier élément (_n-2_). 
* A chaque étape on fixe l'indice qui comporte la valeur minimale comme étant celui à  _i_.
* On parcourt la totalité du reste du tableau pour chercher une valeur plus petite  que celle présente à l'indice _i_
* Si on l'a trouvé : on échange les valeurs contenues à _i_ et celle à l'indice trouvé.

> 📢 Comme le tri par insertion, le tri par sélection est un __tri en place__.


## Un exemple

Voici les étapes de l'exécution du tri par insertion sur le tableau `[18, 3, 10, 25, 9, 3, 11, 13, 23, 8]`. Les indices `i` et `m` sont représentés à la fin de la boule POUR intérieure à chaque étape.[^3]

![Exemple de déroulé pour le tri par selection](./fig/deroule_selection.gif)


## Coût temporel

L'évolution du temps d'exécution ne dépend pas cette fois-ci de la nature du tableau à traiter (partiellement trié ou non) car lors d'un tri par sélection on compare :

* (n-1) éléments au premier tour
* (n-2) au second
* etc... et 1 au dernier

soit  $(n-1)+(n-2)+\dots +1$  comparaisons dont on peut montrer que c'est égal à $n^{2} - \dfrac{n}{2}$.  
Là encore le temps d'exécution varie comme le carré du nombre de données à traiter mais cette fois-ci __dans tous les cas__.

> 📢 L'algorithme de tri par sélection a un  __coût temporel quadratique__ quelque soit le tableau à tier.

On peut mesurer les temps d'exécution du tri par sélection sur des tableaux de tailles différentes contenant des valeurs aléatoires[^4]

![Efficacité du tri par sélection](./fig/temps_tri_selec.jpg)

## Preuve de la terminaison

L'algorithme utilise deux boucles bornées : elles se terminent nécessairement _(voir remarques précédentes)_


## Preuve de la correction

> 📢 Un invariant de boucle pour le tri par selection est donné par la propriété: __"La liste `t[0:i]` est triée"__

* __INITIALISATION__ : à i = 0, la liste`t[0:i]` est triée puisqu'elle ne contient aucun élément : `t[0:0]` correpond à la liste vide
* __CONSERVATION__ : Si au début de l'étape i la liste `t[0:i]` est triée alors la liste `t[0:i+1]`est triée à la fin de l'étape. En effet à chaque tour de la boucle extérieure POUR on cherche la valeur minimale située dans le sous-tableau `t[i:n+1]` et on la place à la position i.
* __TERMINAISON__ : A la fin de la boucle extérieure `i = n-2` et donc la tranche `t[0:n-1]`(c'est à dire la liste sans le dernier élément) est triée. La valeur de ce dernier élément placé à `t[n-1]` est nécessairement plus grande ou égal que `t[n-2]` puisque le minimum entre les deux dernières valeurs a été positionné en `t[n-1]`. La liste complète est donc triée.

L'algorithme est donc correct.

# Beaucoup d'autres tris...

Il existe un grand nombre d'autres algorithmes de tris adaptés à des situations diverses : le tableau est déjà partiellement trié ou non, il est de petite taille ou non....
On citera par exemple: le tri à bulles, le tri fusion, le tri rapide, le tri à peigne ....

Pour plus d'informations : http://lwh.free.fr/pages/algo/tri/tri.htm

Python fournit également des fonctions permettant de trier de manière efficace les éléments d'un tableau :

* Soit on souhaite __trier en place__ (comme pour les tris insertion et selection) : on peut alors utiliser la méthode `sort` sur un tableau

```python
>>> tab = [2, 55, 34, 8, 13, 10 , 100]
>>> tab.sort()
>>> tab
[2, 8, 10, 13, 34, 55, 100]
```

* Soit on ne souhaite pas mofifier le tableau initial, on utilisera alors la fonction `sorted` acceptant un tableau en paramètre et renvoyant un _nouveau tableau_ trié :

```python
>>> tab = [2, 55, 34, 8, 13, 10 , 100]
>>> sorted(tab)
[2, 8, 10, 13, 34, 55, 100]
```

[^1]: https://www-npa.lip6.fr/~blin/Enseignement_files/INAL/INAL_3.pdf
[^2]: https://fr.wikipedia.org/wiki/Tri_par_insertion
[^3]: https://www.enseignement.polytechnique.fr/profs/informatique/Francois.Morain/TC/X2001/Poly/www-poly009.html
[^4]: Numérique et sciences informatiques (_Balabonski, Conchon, Filiâtre, Nguyen_)




site_name: 1ere NSI
site_url: https://patth.frama.io/nsi1ere
repo_url: https://framagit.org/patth/nsi1ere
site_dir: public

markdown_extensions:
    - pymdownx.highlight
    - pymdownx.tilde
    - pymdownx.superfences
    - admonition
    - footnotes # notes de pied de page [^n] 
    - pymdownx.arithmatex:
        generic: true # rendu LateX
    - toc:
        baselevel: 2 # permet d'intégrer les titres de niveau 1 à la TOC

plugins:
    - macros
    - search

theme: 
  name : material
  custom_dir : overrides/
  features:
        - navigation.instant
        - navigation.tabs
        - navigation.tabs.sticky
        - navigation.top
        - toc.integrate # Intégration de la TOC à gauche
        - header.autohide
    
  
  
extra_javascript: # pour le rendu Latex
  - javascripts/config.js
  - https://polyfill.io/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js
  - javascripts/interpreter.js # console  Pyodide

extra_css :
  - stylesheets/pyoditeur.css # CSS Pyodide


nav:
  - Home : 'index.md'
  - Cours/TP/Activités :
    - Documents / Notebooks / PDF :  'cours_tp.md'
    - 1-Découverte Python :
      - Nombres :
        - cours : 1_decouverte_python/python1_nombres/cours1_manipuler_nombres.md
        - tp : 1_decouverte_python/python1_nombres/tp1_nombres/tp1_nombres.md
      - Fonctions :
        - cours : 1_decouverte_python/python2_fonctions/cours2_fonctions.md
        - tp : 1_decouverte_python/python2_fonctions/tp2_fonctions/tp2_fonctions.md
      - Chaînes de caractères :
        - cours : 1_decouverte_python/python3_strings/cours3_strings.md
        - tp : 1_decouverte_python/python3_strings/tp3_strings/tp3_strings.md
    - 2-Représentation des entiers :
      - Entiers naturels:
        - cours : 2_entiers/c2_p1_naturels/cours1_bases.md
        - tp : 2_entiers/c2_p1_naturels/tp1_bases/tp1_bases.md
      - Entiers relatifs :
        - cours : 2_entiers/c2_p2_relatifs/cours2_entiers_relatifs.md
        - tp : 2_entiers/c2_p2_relatifs/tp_relatifs/tp_relatifs.md
    - 3-Algorithmes et Boucles WHILE:
        - cours : 3_boucles_while/c3_while_algo.md
        - tp : 3_boucles_while/tp_while/tp_boucles_while.md
    - 4-Systèmes d'exploitation :
        - Activité introductive : 4_os/partie1/ac_intro/os_intro.md
        - cours : 4_os/partie1/cours/cours_os.md
        - tp commandes unix: 4_os/partie1/tp_shell/tp_shell.md
    - 5-Booléens et structures conditionnelles :
        - cours : 5_booleens_conditions/cours_booleens_conditions.md
        - tp : 5_booleens_conditions/tp_booleens_conditions/tp_booleens_conditions.md
    - 6-Architecture des ordinateurs :
        - Portes logiques :
          - cours : 6_architecture/c6_p1_portes_logiques/cours_portes_logiques.md
          - tp : 6_architecture/c6_p1_portes_logiques/tp_portes_logiques/tp_portes_logiques.md
        - Modèle de Von Neumann :
          - cours : 6_architecture/c6_p2_von_neumann/cours_vonneumann.md
          - tp : 6_architecture/c6_p2_von_neumann/tp2_assembleur/tp2_assembleur.md
    - 7-Boucles FOR :
        - cours : 7_boucles_bornees/cours_for.md
        - tp : 7_boucles_bornees/tp_for/tp_for.md
    - 8-Recherche séquentielle :
        - cours : 8_recherche_sequentielle/c8_recherche_sequentielle.md
        - tp : 8_recherche_sequentielle/tp_recherche_chaines/tp_recherche_seq.md
    - 9-Listes Python :
        - Découverte :
            - cours : 9_tableaux/c9_p1_listes/cours/c9_p1_cours_listes.md
            - tp : 9_tableaux/c9_p1_listes/tp1_listes/tp1_listes.md
        - Parcours :
            - cours : 9_tableaux/c9_p2_parcours_listes/cours/c9_p2_parcours_listes.md
            - tp : 9_tableaux/c9_p2_parcours_listes/tp2_parcourslistes/tp2_parcourslistes.md
        - Listes en compréhension et listes de listes:
            - cours : 9_tableaux/c9_p3_comprehension_2d/cours/c9_p3_cours_comprehension_listes2d.md
            - tp : 9_tableaux/c9_p3_comprehension_2d/tp3_listes/tp3_listes.md
    - 10-Web HTML/CSS :
        - Prérequis : 10_html_css/prerequis_html_css.md
        - Niveau avancé : 10_html_css/html_css_niveau2.md
    - 11-Spécification et tests :
        - cours : 11_specification_tests/c11_specification_tests.md
        - tp : 11_specification_tests/tp_spe_tests/tp_spe_tests.md
    - 12-Tris :
        - Activité introductive : 12_tris/activite_intro_tris/activite_tris_prof.md
        - tp tri insertion : 12_tris/tp1_tri_insert/tri_insertion_intro.md
        - cours : 12_tris/c12_tris_cours.md
        - tp comparaisons de tris : 12_tris/tp2_comparaisons_tris/tp2_comparaisons_tris.md
    - 13-IHM sur le Web :
        - Javascript : 13_ihm_web/c13_p1_js/c13_decouverte_js.md
        - Programmation événementielle :
            - cours : 13_ihm_web/c13_p2_evenementiel/c13_evenementiel.md
            - tp : 13_ihm_web/c13_p2_evenementiel/tp_evenements/tp_evenements.md
    - 14-Réseaux :
        - Protocole HTTP :
            - tp formulaire : 14_reseaux/c14_p1_http/tp_formulaire/tp_formulaire.md
            - cours : 14_reseaux/c14_p1_http/cours/c14_cours_http.md
            - exercices : 14_reseaux/c14_p1_http/exercices/c14_exos_client_serveur.md
        - Protocole TCP :
            - tp simulation : 14_reseaux/c14_p2_tcp/tp1_filius/tp1_filius.md
            - tp tcp : 14_reseaux/c14_p2_tcp/tp2_tcp/tp2_tcp.md
            - cours : 14_reseaux/c14_p2_tcp/cours/c14_cours_tcp.md
    - 15-Tuples : 
        - cours : 15_tuples/c15_tuples_cours.md
        - tp : 15_tuples/tp/c15_tp.md
    - 16-Représentation des nombres réels :
        - cours : 16_reels/cours_reels.md
        - tp : 16_reels/tp_reels/tp_reels.md
    - 17-Représentation des caractères :
        - Activité introductive : 17_caracteres/ac_intro/c17_caracteres_ac_intro.md
        - cours : 17_caracteres/cours/c17_caracteres_cours.md
        - tp : 17_caracteres/tp_caracteres/c17_tp_caracteres.md
    - 18-Dictionnaires : 
        - cours : 18_dicos/c18_dicos.md
        - tp : 18_dicos/tp/c18_tp_dicos.md
    - 19-Tables :
        - cours : 19_tables/partie1_recherche_tables/cours_p1_tables/c19_p1_csv.md
        - tp : 19_tables/partie1_recherche_tables/tp_p1_tables/tp1_tables.md
    - 20-Recherche Dichotomique :
        - Activité introductive : 20_dichotomie/ac_intro_dichotomie/ac_into_dichotomie.md
        - cours : 20_dichotomie/cours/cours_dichotomie.md
        - tp : 20_dichotomie/tp_dichotomie/tp_dichotomie.md
    - 21-Algorithmes gloutons :
        - cours : 21_algos_gloutons/cours/cours_algo_glouton.md
        - tp : 21_algos_gloutons/tp/tp_algo_glouton.md
    - 22-Algorithmes KNN :
        - cours : 22_knn/cours/cours_knn.md
        - tp : 22_knn/tp/tp_knn.md
  - Mini-Projets : 
      - Jeu de Nim :
          - enonce : mini_projets/projets19_20/nim/mini_projet_1_nim_enconce.md  
          - script support : mini_projets/projets19_20/nim/nim_sujet.py
      - Bataille :
          - enonce : mini_projets/projets19_20/bataille/jeu_bataille.md
          - script support : mini_projets/projets19_20/bataille/jeu_bataille.py
      - Pierre-Feuille-Ciseaux :
          - enonce: mini_projets/projets19_20/pfc/mini3_pfc.md
  - Ressources : ressources/ressources.md